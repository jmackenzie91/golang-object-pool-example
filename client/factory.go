package client

type ClientFactory struct{}

func (c ClientFactory) Create() (GetterById, error) {
	return &SomeClient{}, nil
}
