package client

import (
	"net/http"
	"time"
)

type GetterById interface {
	GetEntityByID(int) Entity
}

type SomeClient struct {
	client http.Client
}

func (s SomeClient) GetEntityByID(id int) Entity {
	time.Sleep(time.Second * 2)
	return SomeEntity{
		id: id,
	}
}
