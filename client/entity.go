package client

type Entity interface {
	GetID() int
}

type SomeEntity struct {
	id int
}

func (s SomeEntity) GetID() int {
	return s.id
}
