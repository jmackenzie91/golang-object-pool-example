package client

import (
	"errors"
	"sync"
)

// All pooled object factories must satisfy this interface
type PooledJobFactory interface {
	Create() (PooledJob, error)
}

type PooledJob interface {
	Process(func())
}

// All pool implementations must satisfy this interface
type Pool interface {
	Get() (PooledJob, error)
	Return(obj PooledJob) error
}

/**
 * Fixed size Object pool
 */
type FixedPool struct {
	// List of available objects to share
	available []GetterById
	// List of in-use objects that are currently reserved
	inUse []GetterById
	// Maximum size permitted
	capacity int
	// For protecting updates
	mu *sync.Mutex
	// For creating the Objects
	factory ClientFactory
}

// NewPool returns an instance of a pool which can then be used to request client objects
func NewPool(capacity int) FixedPool {
	return FixedPool{
		mu:       &sync.Mutex{},
		factory:  ClientFactory{},
		capacity: capacity,
	}
}

// Get
func (p *FixedPool) Get() (GetterById, error) {
	p.mu.Lock()

	var obj GetterById
	var err error

	if len(p.available) == 0 {
		// Make sure we don't exceed capacity
		if len(p.inUse) == p.capacity {
			err = errors.New("fixed Pool reached maximum capacity")
		} else {
			//p.wg.Add(1)
			obj, _ = p.factory.Create()
			p.inUse = append(p.inUse, obj)
		}
	} else {
		// pop
		obj, p.available = p.available[0], p.available[1:]
		//p.wg.Add(1)
		err = nil
		p.inUse = append(p.inUse, obj)
	}

	p.mu.Unlock()

	return obj, err
}

func (p *FixedPool) WaitToGet() (GetterById, error) {
	for {
		o, err := p.Get()
		if err == nil {
			return o, nil
		}
	}
}

func (p *FixedPool) Return(obj GetterById) (err error) {
	p.mu.Lock()
	if idx := findIndex(obj, p.inUse); idx != -1 {
		// Delete at index
		p.inUse = append(p.inUse[:idx], p.inUse[idx+1:]...)
		p.available = append(p.available, obj)
		err = nil
	} else {
		err = errors.New("unrecognized pooled object returned")
	}
	p.mu.Unlock()

	return err
}

func findIndex(target GetterById, slice []GetterById) int {
	for idx, obj := range slice {
		if target == obj {
			return idx
		}
	}
	return -1
}
