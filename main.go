package main

import (
	"fmt"
	"no_vcs/me/go-modules/client"
	"sync"
)

func main() {
	// set up pool with limited number of resources
	p := client.NewPool(4)
	// keep track of currently outstanding jobs
	wg := sync.WaitGroup{}
	// search api for ids 1 to 1000
	for i := 1; i < 1000; i++ {
		// add a job
		wg.Add(1)
		go func(id int) {
			// this function hangs until an object can be found
			cli, _ := p.WaitToGet()
			// entity is returned
			ent := cli.GetEntityByID(id)
			// do something with found entity
			fmt.Println(ent.GetID())
			// return client back into pool to be used agaib
			p.Return(cli)
			// mark this as done
			wg.Done()
		}(i)
	}
	// wait until all jobs have been completed
	wg.Wait()
	fmt.Print("done")
}
