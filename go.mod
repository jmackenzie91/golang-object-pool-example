module no_vcs/me/go-modules

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.0
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.4.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a // indirect
	golang.org/x/sys v0.0.0-20190312061237-fead79001313 // indirect
	google.golang.org/appengine v1.5.0 // indirect
)
